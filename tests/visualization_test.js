import test from "ava";

import { configuration, configurationLayout } from "../src/configuration.js";
import { RadialStackedBarChart } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
let sbc = new RadialStackedBarChart();

// TEST INIT //
test("init", t => {

    t.true(sbc.height === configurationLayout.height);
    t.true(sbc.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 500;
let testData = {
    someKey: { "a": 445, "b": 4, "c": 10 },
    someKey2: { "a": 300, "b": 200, "c": 5 }
};

// initialize
let sbcp = new RadialStackedBarChart(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(sbcp.height === testHeight);
    t.true(sbcp.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
